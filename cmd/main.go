package main

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/yanrishbe/gaming-website/game"
	"gitlab.com/yanrishbe/gaming-website/postgres"
	"gitlab.com/yanrishbe/gaming-website/server"
)

func main() {
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetLevel(logrus.DebugLevel)
	db, err := postgres.New()
	if err != nil {
		logrus.Fatal(err)
	}
	r, err := server.New(game.New(db))
	if err != nil {
		logrus.Fatal(err)
	}
	logrus.Fatal(http.ListenAndServe(":8080", r))
}
