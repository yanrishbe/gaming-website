prebuild:
    curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin vX.Y.Z

build:
	go build -o app /app/cmd/main.go
	golangci-lint run -E golint -E goimports -E misspell
	#golangci-lint run -E golint -E goimports -E misspell


run:
	CONN="user=postgres dbname=gaming_website password=docker2147 host=localhost port=5432 sslmode=disable" go run ./cmd/main.go

db:
	docker run  --rm -e POSTGRES_PASSWORD=docker2147 -e POSTGRES_DB=gaming_website -e POSTGRES_USER=postgres -p 5432:5432 postgres
