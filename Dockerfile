FROM gladkikhartem/build:go
ENV CGO_ENABLED=0
WORKDIR /app

COPY . .

RUN go build -o app ./cmd

RUN golangci-lint run -E golint -E goimports -E misspell