package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"sort"
	"time"

	"github.com/stretchr/testify/assert"
)

type Tester struct {
	c   http.Client
	url string
}

//
func (t Tester) Errorf(f string, params ...interface{}) {
	log.Fatalf(f, params...)
}

type User struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Balance int    `json:"balance"`
}

type ReqPoints struct {
	Points int `json:"points"`
}

type Winner struct {
	ID     int    `json:"userId"`
	Name   string `json:"name"`
	Winner bool   `json:"winner,omitempty"`
}

type Tournament struct {
	ID      int      `json:"id"`
	Name    string   `json:"name"`
	Deposit int      `json:"deposit"`
	Winner  int      `json:"winner,omitempty"`
	Prize   int      `json:"prize"`
	Users   []Winner `json:"users"`
	Status  Status   `json:"status"`
}

type Status string

const (
	Active   Status = "active"
	Finished Status = "finished"
)

var serverURL string

func main() {
	if len(os.Args) != 2 {
		log.Fatal("usage: <app> url")
	}

	url := os.Args[1]
	serverURL = url

	t := Tester{
		c: http.Client{
			Timeout: time.Second * 10,
		},
		url: url}

	t.Test()
}

func (t Tester) Test() {
	t.TestUserTakeFund()
	t.TestTournJoin()
}

func (t Tester) TestUserTakeFund() {
	u := User{
		Name:    "name",
		Balance: 1000,
	}
	u = t.CreateUser(u)
	assert.Equal(t, 700, u.Balance)

	u = t.GetUser(u.ID)
	assert.Equal(t, u.Balance, 700)

	p := ReqPoints{100}

	u = t.Take(u.ID, p)
	assert.Equal(t, 600, u.Balance)

	u = t.Fund(u.ID, p)
	assert.Equal(t, 700, u.Balance)

	t.DeleteUser(u.ID)
}

func (t Tester) TestTournJoin() {
	u1 := User{
		Name:    "first",
		Balance: 1000,
	}
	u2 := User{
		Name:    "second",
		Balance: 1000,
	}
	u3 := User{
		Name:    "third",
		Balance: 1000,
	}
	t1 := Tournament{
		Name:    "tournament",
		Deposit: 100,
	}

	u1 = t.CreateUser(u1)
	u2 = t.CreateUser(u2)
	u3 = t.CreateUser(u3)

	t1 = t.CreateTourn(t1)
	assert.Equal(t, Tournament{
		ID:      t1.ID,
		Name:    "tournament",
		Deposit: 100,
		Users:   []Winner{},
		Status:  Active,
	}, t1)

	t1 = t.JoinTourn(t1.ID, Winner{
		ID: u1.ID,
	})
	u1 = t.GetUser(u1.ID)
	assert.Equal(t, 600, u1.Balance)

	t1 = t.JoinTourn(t1.ID, Winner{
		ID: u2.ID,
	})
	u2 = t.GetUser(u2.ID)
	assert.Equal(t, 600, u2.Balance)

	t1 = t.JoinTourn(t1.ID, Winner{
		ID: u3.ID,
	})
	assert.Equal(t, Tournament{
		ID:   t1.ID,
		Name: "tournament",
		Users: []Winner{{
			ID:   u1.ID,
			Name: u1.Name,
		}, {
			ID:   u2.ID,
			Name: u2.Name,
		}, {
			ID:   u3.ID,
			Name: u3.Name,
		}},
		Deposit: 100,
		Prize:   300,
		Status:  Active,
	}, t1)

	u3 = t.GetUser(u3.ID)
	assert.Equal(t, 600, u3.Balance)

	t1 = t.GetTourn(t1.ID)
	assert.Equal(t, Tournament{
		ID:   t1.ID,
		Name: "tournament",
		Users: []Winner{{
			ID:   u1.ID,
			Name: u1.Name,
		}, {
			ID:   u2.ID,
			Name: u2.Name,
		}, {
			ID:   u3.ID,
			Name: u3.Name,
		}},
		Deposit: 100,
		Prize:   300,
		Status:  Active,
	}, t1)

	t1 = t.FinishTourn(t1.ID)
	ftourn := Tournament{
		ID:   t1.ID,
		Name: "tournament",
		Users: []Winner{{
			ID:   u1.ID,
			Name: u1.Name,
		}, {
			ID:   u2.ID,
			Name: u2.Name,
		}, {
			ID:   u3.ID,
			Name: u3.Name,
		}},
		Deposit: 100,
		Prize:   300,
		Winner:  t1.Winner,
		Status:  Finished,
	}
	for i, v := range ftourn.Users {
		if v.ID == ftourn.Winner {
			ftourn.Users[i].Winner = true
		}
	}
	assert.Equal(t, ftourn, t1)

	winner := t.GetUser(t1.Winner)
	assert.Equal(t, 900, winner.Balance)

	t.DelTourn(t1.ID)
}

func (t Tester) request(method string, url string, input interface{}, dst interface{}) {
	data, err := json.Marshal(input)
	assert.NoError(t, err)

	url = fmt.Sprintf("%s%s", serverURL, url)
	req, err := http.NewRequest(method, url, bytes.NewBuffer(data))
	assert.NoError(t, err)
	req.Header.Set("Content-Type", "application/json")
	resp, err := t.c.Do(req)
	assert.NoError(t, err)
	defer func() {
		err := resp.Body.Close()
		assert.NoError(t, err)
	}()

	if dst == nil {
		return
	}
	err = json.NewDecoder(resp.Body).Decode(&dst)
	assert.NoError(t, err)
}

func (t Tester) CreateUser(u User) User {
	t.request(http.MethodPost, "/user", u, &u)
	return u
}

func (t Tester) Take(id int, p ReqPoints) User {
	var u User
	url := fmt.Sprintf("/user/%v/take", id)
	t.request(http.MethodPost, url, p, &u)
	return u
}

func (t Tester) Fund(id int, p ReqPoints) User {
	var u User
	url := fmt.Sprintf("/user/%v/fund", id)
	t.request(http.MethodPost, url, p, &u)
	return u
}

func (t Tester) GetUser(id int) User {
	var u User
	url := fmt.Sprintf("/user/%v", id)
	t.request(http.MethodGet, url, nil, &u)
	return u
}

func (t Tester) DeleteUser(id int) {
	url := fmt.Sprintf("/user/%v", id)
	t.request(http.MethodDelete, url, nil, nil)
}

func (t Tester) CreateTourn(tourn Tournament) Tournament {
	t.request(http.MethodPost, "/tournament", tourn, &tourn)
	return tourn
}

func (t Tester) GetTourn(id int) Tournament {
	var tourn Tournament
	url := fmt.Sprintf("/tournament/%v", id)
	t.request(http.MethodGet, url, nil, &tourn)
	sort.Slice(tourn.Users, func(i, j int) bool {
		return tourn.Users[i].ID < tourn.Users[j].ID
	})
	return tourn
}

func (t Tester) JoinTourn(tID int, w Winner) Tournament {
	var tourn Tournament
	url := fmt.Sprintf("/tournament/%v/join", tID)
	t.request(http.MethodPost, url, w, &tourn)
	sort.Slice(tourn.Users, func(i, j int) bool {
		return tourn.Users[i].ID < tourn.Users[j].ID
	})
	return tourn
}

func (t Tester) FinishTourn(id int) Tournament {
	var tourn Tournament
	url := fmt.Sprintf("/tournament/%v/finish", id)
	t.request(http.MethodPost, url, nil, &tourn)
	sort.Slice(tourn.Users, func(i, j int) bool {
		return tourn.Users[i].ID < tourn.Users[j].ID
	})
	return tourn
}

func (t Tester) DelTourn(id int) {
	url := fmt.Sprintf("/tournament/%v", id)
	t.request(http.MethodDelete, url, nil, nil)
}
